
const _ = require("lodash");
const luxon = require("luxon");
const DateTime = luxon.DateTime;

var weather = require("./weather data.json");


var result = {
  "forecast": [],
}
// "forecast":[
//   {"date":"4/1/2021","high":68,"low":35},
//   {"date":"4/2/2021","high":65,"low":36},
//   {"date":"4/3/2021","high":64,"low":38}]

var day = null;
var currentHigh = undefined;
var currentLow = undefined;

_.each(weather.data.attributes.forecast, (item) => {
  dt = DateTime.fromISO(item.datetime);

  if (day !== null && dt.day !== day) {
    result.forecast.push({
      "date": dt.toLocaleString(),
      "high": currentHigh,
      "low": currentLow
    })

    currentHigh = undefined;
    currentLow = undefined;
    day = null;
  }

  if (day == null) {
    day = dt.day;
  }

  if (dt.day == day) {
    if (currentHigh == undefined || item.temperature > currentHigh){
      currentHigh = item.temperature;
    }
    if (currentLow == undefined || item.temperature < currentLow){
      currentLow = item.temperature;
    }
  }
})

console.log(result);
